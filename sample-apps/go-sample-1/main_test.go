package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestError404func(t *testing.T) {

	router := setupRouter()
	w := performRequest(router, "GET", "/error404")
	assert.Equal(t, http.StatusNotFound, w.Code)
	fmt.Printf("404 emulator test pass")

}

func TestError503func(t *testing.T) {

	router := setupRouter()
	w := performRequest(router, "GET", "/error503")
	assert.Equal(t, http.StatusServiceUnavailable, w.Code)
	fmt.Printf("503 emulator test pass")
}

func TestHealthFunc(t *testing.T) {

	router := setupRouter()
	w := performRequest(router, "GET", "/health")
	assert.Equal(t, http.StatusOK, w.Code)
	fmt.Printf("Health endpoint responsive")
}
