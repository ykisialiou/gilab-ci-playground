package main

import "github.com/gin-gonic/gin"
import "net/http"

func welcomeFunc(c *gin.Context) {

	firstname := c.DefaultQuery("firstname", "Guest")
	lastname := c.Query("lastname")
	c.String(http.StatusOK, "Hello %s %s", firstname, lastname)
}

func HealthFunc(c *gin.Context) {
	c.String(http.StatusOK, "Ok")
}

func Error404func(c *gin.Context) {
	c.String(http.StatusNotFound, "404 emulator")
}

func Error503func(c *gin.Context) {
	c.String(http.StatusServiceUnavailable, "503 emulator")
}

func setupRouter() *gin.Engine {

	router := gin.Default()

	router.GET("/welcome", welcomeFunc)
	router.GET("/health", HealthFunc)
	router.GET("/error404", Error404func)
	router.GET("/error503", Error503func)

	return router
}

func main() {

	router := setupRouter()
	router.Run(":3000")
}
